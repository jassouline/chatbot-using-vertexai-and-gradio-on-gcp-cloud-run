# Utiliser une image de base Python officielle. Vous pouvez choisir une version spécifique de Python en fonction de vos besoins.
FROM python:3.8-slim

# Définir le répertoire de travail dans le conteneur à /app. Tous les commandes ultérieures seront exécutées dans ce répertoire.
WORKDIR /app

# Copier le fichier requirements.txt dans le répertoire de travail (/app).
COPY requirements.txt .

# Installer les dépendances spécifiées dans requirements.txt.
RUN pip install --no-cache-dir -r requirements.txt

# Copier le reste des fichiers de votre application dans le répertoire de travail (/app).
COPY . .

# Exposer le port sur lequel votre application Gradio s'exécute (par défaut, Gradio s'exécute sur le port 7860).
EXPOSE 8501

# Définir la variable d'environnement pour le numéro de port (facultatif, Gradio utilise 7860 par défaut).
ENV PORT 8501

# Commande pour exécuter l'application lorsque le conteneur démarre.
# Remplacer `main.py` par le nom de votre script si différent.
CMD ["python", "main.py"]
