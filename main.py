import gradio as gr
import vertexai
from vertexai.generative_models import GenerativeModel
import vertexai.preview.generative_models as generative_models
import time

# Configuration initiale
PROJECT_ID = 'genai-use-cases'
LOCATION = 'us-central1'
CHATMODEL = 'gemini-1.0-pro-001'

vertexai.init(project=PROJECT_ID, location=LOCATION)
chat_model = GenerativeModel(CHATMODEL)

parameters = {
    "max_output_tokens": 2048,
    "temperature": 0.8,
    "top_p": 0.8,
}

safety_settings = {
    generative_models.HarmCategory.HARM_CATEGORY_HATE_SPEECH: generative_models.HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
    generative_models.HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT: generative_models.HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
    generative_models.HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT: generative_models.HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
    generative_models.HarmCategory.HARM_CATEGORY_HARASSMENT: generative_models.HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
}

chat = chat_model.start_chat()


def generate_response(message, history):
    # La fonction ajustée pour travailler directement avec message
    # Simulant un délai pour illustrer un traitement asynchrone
    time.sleep(len(message) * 0.05)  # Délai simulant la réponse
    response = chat.send_message(message, generation_config=parameters, safety_settings=safety_settings)
    return response.text


demo = gr.ChatInterface(generate_response,
                        textbox=gr.Textbox(placeholder="Your question", container=False, scale=7),
                        title="🦜🔗 Demo Chat with Gemini",
                        description="ChatModel used: " + CHATMODEL).launch(server_port=8501, server_name="0.0.0.0")
